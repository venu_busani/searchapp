import React from 'react';
import "./SearchResults.css";

const SearchResults  = (props) => {
    const { error, isLoading, items, query } = props;

    let results = items && items.length ? (items.map(item => (
        <div className="card" key={item.id}>
                <h4><b>{item.title}</b></h4> 
                <p>{item.description ? item.description : "No Description"}</p> 
        </div>
    ))) : <h4> No Results Found..</h4>;

    return (
        <> 
            <h4>Search Results for '{query}' :</h4>
            {
                props.error ? <div style={{color: "red"}}>Error: {error.message}</div> 
                    : isLoading ? <div className="loader"></div> 
                    : <div>{results}</div>
            }
        </>
    )
}

export default SearchResults;