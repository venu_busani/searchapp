import React from 'react';
import "./RecentSearches.css";

const RecentSearches  = (props) => {
    const { recentSearches, setSearchQuery } = props;

    return (
        <> 
            {
                recentSearches && recentSearches.length 
                ?   <div className="recentSearchContainer">
                        <h4 style={{color: "white"}}>Recent Searches: </h4>
                            {
                                recentSearches.map((searchItem) => (
                                    <button key={searchItem} onClick={() => setSearchQuery(searchItem)} className="btn success">{searchItem}</button>
                                ))
                            }
                    </div> 
                :   null
            }
        </>
    )
}

export default RecentSearches;