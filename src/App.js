import './App.css';
import Search from './Containers/Search/Search';
import ErrorBoundary from './ErrorBoundary';

function App() {
  return (
    <div className="App">
      <ErrorBoundary>

        <Search />
      </ErrorBoundary>
    </div>
  );
}

export default App;
