import React, { Component } from 'react'
import './Search.css';
import SearchResults from "../../Components/searchResults/SearchResults";
import RecentSearches from "../../Components/recentSearches/RecentSearches";

class Search extends Component {

    constructor(props) {
        super(props)
        this.state = {
            searchQuery: '',
            error: null,
            isLoading: false,
            items: [],
            recentSearches: []
        }
    }



    handleInputChange = (event) => {
        this.setSearchQuery(event.target.value);
    }

    setSearchQuery = (query) => {
        try {
            if(query && query.trim()) {
                this.setState({
                    searchQuery: query.trim()
                }, () => {
                    if (!this.state.isLoading) {
                        this.getSearchResults();
                    }
                })
            } else {
                this.setState({
                    searchQuery: query,
                    items: [],
                })
            }
        }catch(err) {
            console.log(`Error in setSearchQuery: ${err}`)
        }
    }

    updateRecentSearches(query) {
        try {
            let recentSearches = [...this.state.recentSearches];
            let searchIndex = recentSearches.indexOf(query)
            if(searchIndex !== -1) {
                recentSearches.splice(searchIndex,1);
            }

            if(recentSearches.length === 5) {
                recentSearches.pop();   
            }
            recentSearches.unshift(query);
            this.setState({recentSearches: recentSearches})
        }catch (err) {
            console.log(`Error in updateRecentSearches: ${err}`)
        }
        
    }

    getSearchResults = () => {

        this.setState({
            isLoading: true,
        }, () => {
            let query = this.state.searchQuery;
            fetch(`http://gateway.marvel.com/v1/public/series?titleStartsWith=${query}&ts=1&apikey=6223795ffa11fa1547b0132a986b54bf&hash=868e1f88c8149a666222b9ef7108beac`)
                .then(res => res.json())
                .then((res) => {
                    if(res && res.code === 200) {
                        this.updateRecentSearches(query);
                        this.setState({
                            isLoading: false,
                            items: res.data ? res.data.results : [],
                            error: null
                        });
                        if(this.state.searchQuery && query && this.state.searchQuery !== query ){
                            this.getSearchResults(); 
                        }
                    }else {
                        this.setState({
                            isLoading: false,
                            items: [],
                            error: {message: res.status || res.message}
                        });
                    }
                    
                }, (error) => {
                    this.setState({
                        isLoading: false,
                        items: [],
                        error: {message: error.status || error.message}
                    });
                });
        });
    }

    render() {
        const { error, isLoading, items, searchQuery, recentSearches } = this.state;

        return (
            <>
                <div className="search-container">
                    <input className="search-input"
                        aria-label="search"
                        name="search"
                        placeholder="Search for series..."
                        onChange={this.handleInputChange}
                        value={this.state.searchQuery}
                    />
                    <RecentSearches recentSearches={recentSearches} setSearchQuery={this.setSearchQuery} />
                </div>
                { 
                    searchQuery && searchQuery.trim()
                        ? <SearchResults error={error} isLoading={isLoading} items={items} query={searchQuery}/>
                        : null
                }
            </>
        )
    }
}

export default Search